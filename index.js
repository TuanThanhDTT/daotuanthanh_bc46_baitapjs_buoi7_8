var numArr = [];
function themSo(){
    var number = document.querySelector("#number").value*1;
    document.querySelector("#number").value = "";
    //thêm giá trị user nhập vào mảng
    numArr.push(number);
    contentHTML = `<h3>Mảng: [${numArr}]</h3>`;
    document.querySelector("#result").innerHTML = contentHTML;
}

// Tính tổng số dương
function tinhTongSoDuong(){
    var tongSoDuong = 0;
    var count = 0;
    for(var i = 0; i < numArr.length; i++){
        if(numArr[i] > 0){
            var soDuong = numArr[i];
            tongSoDuong += soDuong;
            count++;
        }
    }
    document.querySelector("#result1").innerHTML = `<h3>Tổng số dương: ${tongSoDuong}</h3>`;
    return count;
}

// Đếm số dương
function demSoDuong(){
    var demSoDuong = tinhTongSoDuong();
    document.querySelector("#result2").innerHTML = `<h3>Số dương trong mảng: ${demSoDuong}</h3>`;

}

// Tìm số nhỏ nhất trong mảng
function timSoNhoNhat(){
    var minNumber = numArr[0];
    for(var i = 1; i < numArr.length; i++){
        if(numArr[i] < minNumber){
            minNumber = numArr[i];
        }
    }
    document.querySelector("#result3").innerHTML = `<h3>Số nhỏ nhất trong mảng: ${minNumber}</h3>`;
}

// Tìm số dương nhỏ nhất trong mảng
function timSoDuongNhoNhat(){
    var numArr4 = [];
    for(var i = 0; i < numArr.length; i++){
        if(numArr[i] >= 0){
            numArr4.push(numArr[i]);
        }
    }
    
    if(numArr4.length > 0 ){
        var minNumber4 = numArr4[0];
        for(var i = 1; i < numArr4.length; i++){
            if(numArr4[i] < minNumber4){
                minNumber4 = numArr4[i];
            }
        }
        document.querySelector("#result4").innerHTML = `<h3>Số dương nhỏ nhất: ${minNumber4}</h3>`;
    }else{
        document.querySelector("#result4").innerHTML = `<h3>Trong mảng không có số dương</h3>`;
    }  
}

// Tìm số chẵn cuối cùng trong mảng
function timSoChanCuoiCung(){
    var soChan = 0;
    for(i = 0; i < numArr.length; i++){
        if(numArr[i] % 2 == 0){
            soChan = numArr[i];
        }
    }
    document.querySelector("#result5").innerHTML = `<h3>Số chẵn cuối cùng trong mảng là : ${soChan}</h3>`;
}

// Đổi vị trí các phần tử trong mảng
function doiViTri(){
    var indexValue1 = document.querySelector("#number1").value*1;
    var indexValue2 = document.querySelector("#number2").value*1;
    var temp = numArr[indexValue1];
    numArr[indexValue1] = numArr[indexValue2];
    numArr[indexValue2] = temp;
    document.querySelector("#result6").innerHTML = `<h3>Mảng sau khi đổi vị trí: [${numArr}]</h3>`;
}

//Sắp xếp mảng theo thứ tự tăng dần
function sapXepTangDan(){
    for(i = 0; i < numArr.length - 1; i++){
        for(j = i + 1; j < numArr.length; j++){
            if(numArr[i] > numArr[j]){
                var temp = numArr[i];
                numArr[i] = numArr[j];
                numArr[j] = temp;
            }
        }
    }
    document.querySelector("#result7").innerHTML = `<h3>Mảng sau khi đổi sắp xếp: [${numArr}]</h3>`;
}

//Tìm số nguyên tố đầu tiên trong mảng. Nếu mảng không có số nguyên tố thì trả về – 1.
function kTSoNguyenTo(n){
    // Biến cờ hiệu
    var flag = true;
    // Nếu n bé hơn 2 tức là không phải số nguyên tố
    if (n < 2){
        flag = false;
    }
    else if (n == 2){
        flag = true;
    }
    else if (n % 2 == 0){
      flag = false;
    }
    else{
        // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
        for (var i = 3; i < Math.sqrt(n); i+=2)
        {
            if (n % i == 0){
                flag = false;
                break;
            }
        }
    }
    return flag;
}
function timSoNguyenTo(){
    var soNguyenToDauTien = -1;
    for(i = 0; i < numArr.length; i++){
        if(kTSoNguyenTo(numArr[i])){
            soNguyenToDauTien = numArr[i];
            break;
        }
    }
    document.querySelector("#result8").innerHTML = `<h3>Số nguyên tố đầu tiên là: ${soNguyenToDauTien}</h3>`;
}

//Nhập thêm 1 mảng số thực, tìm xem trong mảng có bao nhiêu số nguyên?
var numArr9 = [];
function themSoR(){
    var number9 = document.querySelector("#number9").value*1;
    document.querySelector("#number9").value = "";
    //thêm giá trị user nhập vào mảng
    numArr9.push(number9);
    contentHTML = `<h3>Mảng số thực: [${numArr9}]</h3>`;
    document.querySelector("#result9").innerHTML = contentHTML;
}

function demSoNguyen(){
    var count = 0;
    for(var i = 0; i < numArr9.length; i++){
        if(Number.isInteger(numArr9[i])){
            count++;
        }
    }
    document.querySelector("#result9_1").innerHTML = `<h3>Số nguyên: ${count}</h3>`;
}

// So sánh số lượng số dương và số lượng số âm xem số nào nhiều hơn.
function soSanh(){
    var tongSoDuong = tinhTongSoDuong();
    var tongSoAm = 0;
    for(var i = 0; i < numArr.length; i++){
        if(numArr[i] < 0){
            tongSoAm++;
        }
    }
    if(tongSoDuong > tongSoAm){
        document.querySelector("#result10").innerHTML = `<h3>Số dương > Số âm</h3>`
    }else if(tongSoDuong < tongSoAm){
        document.querySelector("#result10").innerHTML = `<h3>Số dương < Số âm</h3>`
    }else{
        document.querySelector("#result10").innerHTML = `<h3>Số dương = Số âm</h3>`
    }
}


